This project utilizes HTML/CSS/JS to create an interactive Rock-Paper-Scissors Game.

From The Odin Project's [curriculum](https://www.theodinproject.com/courses/web-development-101/lessons/rock-paper-scissors)