let userScore = 0;
let compScore = 0;
const message_h2 = document.querySelector("#message");
const userScore_span = document.getElementById("player-score");
const compScore_span = document.getElementById("computer-score");
const scoreBoard_div = document.querySelector("leader-board");
const rock_div = document.getElementById("r");
const paper_div = document.getElementById("p");
const scissors_div = document.getElementById("s");
const resetBtn_button = document.getElementById("resetBtn")



//  computers choise


function getComputerChoise() {
    const choices = ['r', 'p', 's'];
    const randomNumber = Math.floor(Math.random() * 3);
    return choices [randomNumber];
    
}


//  Converts letters r,p,s from players choise output

function converter(letter) {
    if (letter === "r") {
        return "Rock";
    } if (letter === "p") {
        return "Paper"
    } else {
        return "Scissors"
    }
    
           
}

// win, lose and Draw functions with score update and message

function win(userChoise, computerChoise) {
    userScore++
    if (userScore === 5) {
        gameEnd();
        
    } else {
    const userChoise_div = document.getElementById(userChoise)
    userScore_span.innerHTML = userScore;
    message_h2.innerHTML = `You Win! ${converter(userChoise)} beats ${converter(computerChoise)}.`
    userChoise_div.classList.add('green-glow')
    setTimeout(() => userChoise_div.classList.remove('green-glow'), 500);
    }


}

function lose(userChoise, computerChoise) {
    compScore++
    if (compScore === 5) {
        gameEnd();
        
    } else {
    const userChoise_div = document.getElementById(userChoise)
    compScore_span .innerHTML = compScore;
    message_h2.innerHTML = `You Lost! ${converter(userChoise)} beats ${converter(computerChoise)}.`
    userChoise_div.classList.add('red-glow')
    setTimeout(() => userChoise_div.classList.remove('red-glow'), 500);
    }

}

function draw(userChoise, computerChoise) {
    const userChoise_div = document.getElementById(userChoise)
    message_h2.innerHTML = `Draw! ${converter(userChoise)} equals ${converter(computerChoise)}.`
    userChoise_div.classList.add('grey-glow')
    setTimeout(() => userChoise_div.classList.remove('grey-glow'), 500);

}

//  round

function game(userChoise) {
   const computerChoise = getComputerChoise();
   switch (userChoise + computerChoise) {
       case "rs":
       case "pr":
       case "sp":
           win(userChoise, computerChoise);
           break;
        case "sr":
        case "rp":
        case "ps":
            lose(userChoise, computerChoise);
            break;
        case "rr":
        case "pp":
        case "ss":
            draw(userChoise, computerChoise);
            break;
   }
    
}


//  Players move selection


function main(){

    rock_div.addEventListener('click', () => game("r"))
    paper_div.addEventListener('click', () => game("p"))
    scissors_div.addEventListener('click', () =>game("s"))
}





main();



function resetGame(){
    resetBtn_button.addEventListener("click", function() {
        userScore_span.innerHTML = 0;
        compScore_span .innerHTML = 0;
        message_h2.innerHTML = "Make Your Choise!"
        userScore = 0;
        compScore = 0;
    })
}
resetGame()

/* Determines who won the game */

function gameEnd () {
    if (userScore === 5) {
        message_h2.innerHTML = "You won! You're unstoppable!";
        startOver();
    }
    if (compScore === 5) {
        message_h2.innerHTML = "You lost! Better luck next time!";
        startOver();
    }
};

gameEnd()

function startOver() {
        userScore_span.innerHTML = 0;
        compScore_span .innerHTML = 0;
        userScore = 0;
        compScore = 0;
}


